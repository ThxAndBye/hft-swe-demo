# This file is a template, and might need editing before it works on your project.
---
# Build JAVA applications using Apache Maven (http://maven.apache.org)
# For docker image tags see https://hub.docker.com/_/maven/
#
# For general lifecycle information see https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html
#
# This template will build and test your projects as well as create the documentation.
#
# * Caches downloaded dependencies and plugins between invocation.
# * Verify but don't deploy merge requests.
# * Deploy built artifacts from master branch only.
# * Shows how to use multiple jobs in test stage for verifying functionality
#   with multiple JDKs.
# * Uses site:stage to collect the documentation for multi-module projects.
# * Publishes the documentation for `master` branch.

image: docker:latest
services:
  - docker:dind

variables:
  # This will supress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used
  # when running from the command line.
  # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"
  DOCKER_DRIVER: overlay
  SPRING_PROFILES_ACTIVE: gitlab-ci
  BUILD_TAG: '2'

# Cache downloaded dependencies and plugins between builds.
# To keep cache across branches add 'key: "$CI_JOB_REF_NAME"'
cache:
  paths:
    - .m2/repository

stages:
    - build
    - test
    - package
    - pre-deploy
    - deploy

# This will only validate and compile stuff and run e.g. maven-enforcer-plugin.
# Because some enforcer rules might check dependency convergence and class duplications
# we use `test-compile` here instead of `validate`, so the correct classpath is picked up.
.validate: &validate
  stage: build
  script:
    - 'mvn $MAVEN_CLI_OPTS test-compile'

# For merge requests do not `deploy` but only run `verify`.
# See https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html
.verify: &verify
  stage: test
  script:
    - 'mvn $MAVEN_CLI_OPTS verify'
  except:
    - master

# Validate merge requests using JDK8
validate:jdk8:
  <<: *validate
  image: maven:3.3.9-jdk-8

# For `master` branch run `mvn deploy` automatically.
# Here you need to decide whether you want to use JDK7 or 8.
# To get this working you need to define a volume while configuring your gitlab-ci-multi-runner.
# Mount your `settings.xml` as `/root/.m2/settings.xml` which holds your secrets.
# See https://maven.apache.org/settings.html
deploy:jdk8:
  # Use stage test here, so the pages job may later pickup the created site.
  stage: test
  script:
    - 'mvn $MAVEN_CLI_OPTS install site'
  only:
    - master
  # Archive up the built documentation site.
  artifacts:
    paths:
    - target/*.jar
    - target/site
  image: maven:3.3.9-jdk-8

docker-build:
  stage: package
  script:
  - docker build -t $CI_REGISTRY_IMAGE:$BUILD_TAG .
  - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
  - docker push $CI_REGISTRY_IMAGE

pages:
  image: busybox:latest
  stage: deploy
  script:
    # Because Maven appends the artifactId automatically to the staging path if you did define a parent pom,
    # you might need to use `mv target/staging/YOUR_ARTIFACT_ID public` instead.
    - mv target/site public
  dependencies:
    - deploy:jdk8
  artifacts:
    paths:
    - public
  only:
    - master

prep-k8s-deploy:
  image: google/cloud-sdk
  environment:
      name: dev-greeting-app
  stage: pre-deploy
  script:
    - echo "$GOOGLE_KEY" > key.json
    - gcloud auth activate-service-account --key-file key.json
    - gcloud config set compute/zone us-central1-a
    - gcloud config set project $CI_PROJECT_NAME
    - kubectl delete secret registry.gitlab.com --namespace=$CI_PROJECT_NAME
  allow_failure: true

k8s-deploy:
  image: google/cloud-sdk
  environment:
      name: dev-greeting-app
  stage: deploy
  script:
  - echo "$GOOGLE_KEY" > key.json
  - gcloud auth activate-service-account --key-file key.json
  - gcloud config set compute/zone us-central1-a
  - gcloud config set project $CI_PROJECT_NAME
  ### Replace vars in deployment.yml ###
  - sed -i s,{{BUILD_TAG}},$BUILD_TAG,g deployment.yml
  - sed -i s,{{CI_ENVIRONMENT_SLUG}},$CI_ENVIRONMENT_SLUG,g deployment.yml
  - sed -i s,{{CI_PROJECT_NAME}},$CI_PROJECT_NAME,g deployment.yml
  - sed -i s,{{CI_REGISTRY_IMAGE}},$CI_REGISTRY_IMAGE,g deployment.yml
  ### Replace vars in service.yml ###
  - sed -i s,{{CI_ENVIRONMENT_SLUG}},$CI_ENVIRONMENT_SLUG,g service.yml
  - sed -i s,{{CI_PROJECT_NAME}},$CI_PROJECT_NAME,g service.yml
  ### Replace vars in namespace.yml ###
  - sed -i s,{{CI_PROJECT_NAME}},$CI_PROJECT_NAME,g namespace.yml
  - kubectl apply -f namespace.yml
  - kubectl create secret docker-registry registry.gitlab.com --docker-server=https://registry.gitlab.com --docker-username=$CI_REGISTRY_USER --docker-password=$CI_REGISTRY_PASSWORD --docker-email=$GITLAB_USER_EMAIL --namespace=$CI_PROJECT_NAME
  - kubectl apply -f deployment.yml
  - kubectl apply -f service.yml

